package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	/*
	 * Comparator based on brand, model, and options.
	 */
	@Override
	public int compare(Car c1, Car c2) {
		
		int flag = c1.getCarBrand().compareTo(c2.getCarBrand());
		
		if(flag > 0) {
			return 1;
		}
		else if(flag < 0)
			return -1;
		
		flag = c1.getCarModel().compareTo(c2.getCarModel());
		
		if(flag > 0)
			return 1;
		else if(flag < 0)
			return -1;
		
		flag = c1.getCarModelOption().compareTo(c2.getCarModelOption());
		
		if(flag > 0)
			return 1;
		else if (flag < 0)
			return -1;
		
		return 0;
	}

}

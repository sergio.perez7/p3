package edu.uprm.cse.datastructures.cardealer.util;

import java.io.PrintStream;
import java.util.Comparator;

public class TwoThreeTree<K,V> extends BTree<K,V> {

	Comparator<V> comp;
	
	public TwoThreeTree(Comparator<K> keyComparator) { super(keyComparator); }

	@Override	
	public int size() { return currentSize; }

	@Override
	public boolean isEmpty() { return size() == 0; }

	@Override
	public boolean contains(K key) {  return get(key) != null; }

	@Override
	public V get(K key) {
		if(key == null)
			throw new IllegalArgumentException("Key cannot be null");
		if(isEmpty())
			return null;
		return getAux(root,key);
	}
	
	private V getAux(TreeNode N, K key) {
		if(N == null)
			return null;
		else {
			
			MapEntry mE1 = N.entries.first();
			MapEntry mE2 = N.entries.last();
			
			if(!mE1.deleted && mE1.key.equals(key))
				return mE1.value;
			if(!mE2.deleted && mE2.key.equals(key))
				return mE2.value;
			
			int comparison1 = keyComparator.compare(key, mE1.key);	// Key compared w/ left
			int comparison2 = keyComparator.compare(key, mE2.key);	// Key compared w/ right
			
			if(comparison1 <= 0)				// In the Left
				return getAux(N.left, key);
			else if(comparison2 > 0)			// In the Right
				return getAux(N.right, key);
			else								// In the Middle.
				return getAux(N.center, key);
		}
	}

	@Override
	public V put(K key, V value) {
		if(key == null || value == null)
			throw new IllegalArgumentException("Key or Value cannot be null.");
		if(isEmpty()) {
			root = new TreeNode(new MapEntry(key, value, keyComparator), null,keyComparator);
			currentSize++;
			return value;
		}
		if(this.contains(key))
			remove(key);
		return putAux(root,key,value);
	}
	
	private V putAux(TreeNode N, K key, V value) {

		if(isLeaf(N)) {
			MapEntry mE = new MapEntry(key,value,keyComparator);
			N.entries.add(mE);
			currentSize++;
			if(N.entries.size() == 3)
				split(N);
		}
		else {
		
			int comparisonA = keyComparator.compare(key, N.entries.first().key); ;
			int comparisonB;
			
			if(comparisonA <= 0)
				return putAux(N.left, key, value);
			
			if(N.entries.first() != N.entries.last()) {
				comparisonB = keyComparator.compare(key, N.entries.last().key);
				return comparisonB > 0 ? putAux(N.right, key, value) : putAux(N.center, key, value);
			}
			else
				return putAux(N.right, key, value);
		}
		return value;
	}

	@Override
	void split(TreeNode N) {

		if(N.entries.size() != 3)
			return;
		else if(N == root) {
			
			TreeNode newRoot 	= new TreeNode(root.entries.get(1), null, keyComparator);
			newRoot.left 		= new TreeNode(root.entries.get(0), null, keyComparator);
			newRoot.right 		= new TreeNode(root.entries.get(2), null, keyComparator);
			newRoot.left.parent = newRoot;
			newRoot.right.parent = newRoot;
			
			// TODO: LOSING NODES HERE I THINK.
			// CHECK FOR TEMP
			if(root.left != null || root.right != null) {
//				root.center.parent = newRoot;
//				root.right.parent = newRoot;
			}

			root = newRoot;
		}
		else {

			N.parent.entries.add(N.entries.get(1));								// Middleside Entry to N.parent
			N.entries.remove(1);
			
				if(N.parent.entries.first().compareTo(N.entries.first()) < 0) {	// Leftside Entry goes to Center Node
					if(N.parent.center == null) {
						N.parent.center = new TreeNode(N.entries.first(), null, keyComparator);
						N.parent.center.parent = N.parent;
					}
					else
						N.parent.center.entries.add(N.entries.first());
					N.entries.remove(0);	
				}
				else {															// Rightside Entry
					if(N.parent.center == null) {
						N.parent.center = new TreeNode(N.entries.last(), null, keyComparator);
						N.parent.center.parent = N.parent;
					}
					else
						N.parent.center.entries.add(N.entries.last());
					N.entries.remove(N.entries.size()-1); 
				}
			split(N.parent);
		}
	}	

	@Override
	public V remove(K key) { 
		if(key == null)
			throw new IllegalArgumentException("Key cannot be null.");
		return removeAux(root, key);
	}
	
	private V removeAux(TreeNode N, K key) {
		if(N == null)
			return null;
		else {
			
			MapEntry mE1 = N.entries.first();
			MapEntry mE2 = N.entries.last();
			
			if(!mE1.deleted && mE1.key.equals(key)) {
				V result = mE1.value;
				mE1.deleted = true;
				currentSize--;
				return result;
			}

			if(!mE2.deleted && mE2.key.equals(key)) {
				V result = mE2.value;
				mE2.deleted = true;
				currentSize--;
				return result;
			}
			
			int comparison1 = keyComparator.compare(key, mE1.key);	// Key compared w/ left
			int comparison2 = keyComparator.compare(key, mE2.key);	// Key compared w/ right
			
			if(comparison1 <= 0)				// In the Left
				return removeAux(N.left, key);
			else if(comparison2 > 0)			// In the Right
				return removeAux(N.right, key);
			else								// In the Middle.
				return removeAux(N.center, key);
		}
	}

	@Override
	public SortedList<K> getKeys() {
		SortedList<K> result = new CircularSortedDoublyLinkedList<>(keyComparator);
		getKeysAux(root,result);
		return result;
	}
	
	private void getKeysAux(TreeNode N, SortedList<K> result) {
		if(N == null)	
			return;
		else {
			for(MapEntry mE : N.entries)
				if(!mE.deleted)
					result.add(mE.key);
			getKeysAux(N.left, result);
			getKeysAux(N.center, result);			
			getKeysAux(N.right, result);			
		}
	}
	
	/*
	 * "El metodo de getValues() va a devolver la lista ordenada por keys,
	 * en CarManager necesitas hacer el cambio necesario para que puedas 
	 * ordenarlos por Car y devolverlo." - Luis
	 */
	public SortedList<V> getValues() { 	
		SortedList<V> result = new CircularSortedDoublyLinkedList<V>(comp);
		getValuesAux(root, result);
		return result;
	}
	
	private void getValuesAux(TreeNode N, SortedList<V> result) {
		if(N == null)	
			return;
		else {
			for(MapEntry mE : N.entries)
				if(!mE.deleted)
					result.add(mE.value);
			getValuesAux(N.left, result);
			getValuesAux(N.center, result);			
			getValuesAux(N.right, result);			
		}
	}

	@Override
	public void clear() { 
		this.root = null;
		currentSize = 0;
		System.gc();
	}
	
	@Override
	public void print(PrintStream out) { printAux(root, 0); }

	@Override
	boolean isLeaf(TreeNode N) { return N.left == null && N.center == null && N.right == null & N.temp == null; }

	public void setValueComparator(Comparator<V> comp) { this.comp = comp; }	
}
package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.BTree;
import edu.uprm.cse.datastructures.cardealer.util.TwoThreeTree;

public class CarTable {

	private static TwoThreeTree<Long, Car> list = new TwoThreeTree<>(new LongComparator());
	
	public static TwoThreeTree<Long, Car> getInstance() { return list; }
	public static void resetCars() { CarTable.getInstance().clear(); }
}
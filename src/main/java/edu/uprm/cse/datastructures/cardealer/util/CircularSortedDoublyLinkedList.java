package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;

import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private int currentSize;
	private Node<E> header;
	private Comparator<E> comp;
	
	public CircularSortedDoublyLinkedList() {
		currentSize = 0;
		header = new Node<E>();
	}
	
	public CircularSortedDoublyLinkedList(Comparator<E> comp) { 
		this();
		this.comp = comp;
	}
	
	@Override
	public boolean isEmpty() {  return size() == 0; }

	@Override
	public int size() { return currentSize; }

	public Iterator<E> iterator() { return new CircularSortedDoublyLinkedListIterator<E>(); }
	
	@Override
	public E first() { return isEmpty() ? null : header.getNext().getElement(); }

	@Override
	public E last() { return isEmpty() ? null : header.getPrev().getElement(); }
	
	@Override
	public boolean add(E e) {
		
		if(e == null)
			return false;

		if(isEmpty()) {
			Node<E> newNode = new Node<E>(e, header, header);
			header.setNext(newNode); header.setPrev(newNode);
		}
		else {
			
			Node<E> temp = header.getNext();

			while(comp.compare(e, temp.getElement()) > 0) {
				temp = temp.getNext();
				if(temp.equals(header))
					break;
			}
			
			Node<E> newNode = new Node<>(e, temp, temp.getPrev());
			temp.getPrev().setNext(newNode);
			temp.setPrev(newNode);
		}
		currentSize++;
		return true;
	}

	@Override
	public boolean remove(E e) {
		
		if(e == null)
			return false;
		
		int i = firstIndex(e);
		
		if(i < 0)
			return false;
		
		remove(i);
		return true;
	}

	@Override
	public boolean remove(int index) {
		if(index < 0 || index > size()) {
			return false; // throw new IndexOutOfBoundsException();
		}
		
		Node<E> node = getPosition(index);
		node.getNext().setPrev(node.getPrev());
		node.getPrev().setNext(node.getNext());
		
		node.setElement(null); node.setNext(null); node.setPrev(null);
		
		currentSize--;
		return true;
	}
	
	private Node<E> getPosition(int index){
		
		int currentPosition = 0;
		Node<E> temp = header.getNext();
		
		while(currentPosition < index) {
			temp = temp.getNext();
			currentPosition++;
		}
		
		return temp;
	}

	@Override
	public int removeAll(E e) {
		
		int i = 0;
		while(remove(e))
			i++;
		
		return i;
	}

	@Override
	public E get(int index) {
		if(index < 0 || index > size())
			throw new IndexOutOfBoundsException();
		
		return isEmpty() ? null : getPosition(index).getElement();		
	}

	@Override
	public void clear() { while(!isEmpty()) remove(0); }

	@Override
	public boolean contains(E e) {  return firstIndex(e) >= 0 ? true : false; }

	@Override
	public int firstIndex(E e) {
		if(isEmpty())
			return -1;
		
		int i = 0;
		Node<E> temp = header.getNext();
		
		while(temp != header) {
			if(temp.getElement().equals(e))
				return i;
			i++;
			temp = temp.getNext();
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {		
		if(isEmpty())
			return -1;
		
		int i = size()-1;
		Node<E> temp = header.getPrev();
		
		while(temp != header) {
			if(temp.getElement().equals(e))
				return i;
			--i;
			temp = temp.getPrev();
		}
		
		return -1;
	}

	private static class Node<E> {
		private E element;
		private Node<E> next, prev;
		
		public Node(E element, Node<E> next, Node<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev = prev;
		}

		public Node() { super(); }
		
		public E getElement() { return element; }
		public Node<E> getNext() { return next; }
		public Node<E> getPrev() { return prev; }
		
		public void setElement(E element) { this.element = element; }
		public void setNext(Node<E> next) { this.next = next; }
		public void setPrev(Node<E> prev) { this.prev = prev; }		
	}

	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E> {

		private Node<E> nextNode;
		
		@SuppressWarnings("unchecked")
		public CircularSortedDoublyLinkedListIterator() { nextNode = (Node<E>) header.getNext(); }
		
		@Override
		public boolean hasNext() { return nextNode != header; }

		@Override
		public E next() {
			if(!hasNext())
				throw new NoSuchElementException();
			E e = nextNode.getElement();
			nextNode = nextNode.getNext();
			return e;
		}	
	}
}
package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {


	/*
	 * "El metodo de getValues() va a devolver la lista ordenada por keys,
	 * en CarManager necesitas hacer el cambio necesario para que puedas 
	 * ordenarlos por Car y devolverlo." - Luis
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() { 
		
		Car[] cars = new Car[CarTable.getInstance().size()];	// As as this turns to size 2 it fucks up.
		SortedList<Car> sortedCars = new CircularSortedDoublyLinkedList<>(new CarComparator());
		CarTable.getInstance().setValueComparator(new CarComparator());
		
		for (Car car : CarTable.getInstance().getValues())
			sortedCars.add(car);
		for (int i = 0; i < sortedCars.size(); i++)
			cars[i] = sortedCars.get(i);
		
		return cars;
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		
		Car car = CarTable.getInstance().get(id);

		if (car == null) {
			System.out.println("Car not found in getCar().");
			throw new NotFoundException("Error Car " + id + " not found");
		} else
			return car;
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {

		if (car == null)
			throw new IllegalArgumentException("Car cannot be null");

		CarTable.getInstance().put(car.getCarId(), car);
		System.out.println("POST: " + car);
		return Response.status(Response.Status.CREATED).build();
	}

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(Car car) {

		if (car == null)
			throw new IllegalArgumentException("Car cannot be null");

		CarTable.getInstance().put(car.getCarId(), car);
		return Response.status(Response.Status.OK).build();
	}

	@DELETE
	@Path("{id}/delete")
	public Response delete(@PathParam("id") long id) {

		Car car = CarTable.getInstance().remove(id);
		if (car == null) {
			System.out.println("Car not found in delete().");
			return Response.status(Response.Status.NOT_FOUND).build();
		} else {
			return Response.status(Response.Status.OK).build();
		}
	}
}